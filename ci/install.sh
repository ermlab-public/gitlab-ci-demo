#!/usr/bin/env bash
echo "--- ci/install.sh ---"

echo "--- apt update ---"
apt-get -yqq update

echo "--- apt install OS deps ---"
apt-get -yqq install python3-pip python3-dev python3-setuptools git supervisor

echo "--- update pip & install pip deps ---"
pip install --upgrade pip
pip install -r requirements.txt